package com.example.progmob.Pertemuan2;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.example.progmob.Adapter.MahasiswaRecyclerAdapter;
import com.example.progmob.Model.Mahasiswa;
import com.example.progmob.R;

import java.util.ArrayList;
import java.util.List;

public class RecyclerActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler);

        RecyclerView rv = (RecyclerView)findViewById(R.id.rvLatihan);
        MahasiswaRecyclerAdapter mahasiswaRecyclerAdapter;

        //data dumy
        List<Mahasiswa> mahasiswaList = new ArrayList<Mahasiswa>();

        //generate data mahasiswa
        Mahasiswa m1 = new Mahasiswa ("Anisya Clara", "72180260", "081354678909");
        Mahasiswa m2 = new Mahasiswa ("Efi Vania", "72180261", "081354678904");
        Mahasiswa m3 = new Mahasiswa ("Asima", "72180262", "081354678903");
        Mahasiswa m4 = new Mahasiswa ("Griselda Lonefa", "72180263", "081354678900");
        Mahasiswa m5 = new Mahasiswa ("Rani Abigael", "72180264", "081354678901");

        mahasiswaList.add(m1);
        mahasiswaList.add(m2);
        mahasiswaList.add(m3);
        mahasiswaList.add(m4);
        mahasiswaList.add(m5);

        mahasiswaRecyclerAdapter = new MahasiswaRecyclerAdapter(RecyclerActivity.this);
        mahasiswaRecyclerAdapter.setMahasiswaList(mahasiswaList);

        rv.setLayoutManager(new LinearLayoutManager(RecyclerActivity.this));
        rv.setAdapter(mahasiswaRecyclerAdapter);
    }
}