package com.example.progmob.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.progmob.Model.Dosen;
import com.example.progmob.R;

import java.util.ArrayList;
import java.util.List;

public class DosenCRUDRecyclerAdapter extends RecyclerView.Adapter<DosenCRUDRecyclerAdapter.ViewHolder> {
    private Context context;
    private List<Dosen> dosenList;
    public List<Dosen> getDosenList() {
        return dosenList;
    }

    public void setDosenList(List<Dosen> dosenList) {
        this.dosenList = dosenList;
        notifyDataSetChanged();
    }

    public DosenCRUDRecyclerAdapter(Context context) {
        this.context = context;
        dosenList = new ArrayList<>();
    }
    public DosenCRUDRecyclerAdapter(List<Dosen> dosenList){
        this.dosenList = dosenList;
    }

    @NonNull
    @Override
    public DosenCRUDRecyclerAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_dosen,parent,false);
        return new DosenCRUDRecyclerAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull DosenCRUDRecyclerAdapter.ViewHolder holder, int position) {
        Dosen d = dosenList.get(position);

        holder.textNama.setText(d.getNama());
        holder.textNidn.setText(d.getNidn());
        holder.textAlamat.setText(d.getAlamat());
        holder.textEmail.setText(d.getEmail());
        holder.textGelar.setText(d.getGelar());
        //holder.textTelp.setText(m.getNotelp());
    }

    @Override
    public int getItemCount() {

        return dosenList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView textNama, textNidn, textAlamat, textEmail, textGelar;

        public ViewHolder(@NonNull View itemView){
            super(itemView);
            textNama = itemView.findViewById(R.id.textNama);
            textNidn = itemView.findViewById(R.id.textNidn);
            textAlamat = itemView.findViewById(R.id.textAlamat);
            textEmail = itemView.findViewById(R.id.textEmail);
            textGelar = itemView.findViewById(R.id.textGelar);
            //textTelp = itemView.findViewById(R.id.textTelp);
        }
    }

}
