package com.example.progmob.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.progmob.Model.Mahasiswa;
import com.example.progmob.R;

import java.util.ArrayList;
import java.util.List;

public class MahasiswaCRUDRecyclerAdapter extends RecyclerView.Adapter<MahasiswaCRUDRecyclerAdapter.ViewHolder>{
    private Context context;
    private List<Mahasiswa> mahasiswaList;

    public List<Mahasiswa> getMahasiswaList() {

        return mahasiswaList;
    }

    public void setMahasiswaList(List<Mahasiswa> mahasiswaList) {
        this.mahasiswaList = mahasiswaList;
        notifyDataSetChanged();
    }

    public MahasiswaCRUDRecyclerAdapter(Context context) {
        this.context = context;
        mahasiswaList = new ArrayList<>();
    }
    public MahasiswaCRUDRecyclerAdapter(List<Mahasiswa> mahasiswaList){
        this.mahasiswaList = mahasiswaList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_cardview,parent,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Mahasiswa m = mahasiswaList.get(position);

        holder.textNama.setText(m.getNama());
        holder.textNim.setText(m.getNim());
        holder.textAlamat.setText(m.getAlamat());
        holder.textEmail.setText(m.getEmail());
        //holder.textTelp.setText(m.getNotelp());
    }

    @Override
    public int getItemCount() {

        return mahasiswaList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView textNama, textNim, textTelp, textAlamat, textEmail;

        public ViewHolder(@NonNull View itemView){
            super(itemView);
            textNama = itemView.findViewById(R.id.textNama);
            textNim = itemView.findViewById(R.id.textNim);
            textAlamat = itemView.findViewById(R.id.textAlamat);
            textEmail = itemView.findViewById(R.id.textEmail);
            //textTelp = itemView.findViewById(R.id.textTelp);
        }
    }
}
