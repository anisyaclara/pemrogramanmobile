package com.example.progmob.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import java.util.List;

import com.example.progmob.Model.MahasiswaDebugging;
import com.example.progmob.R;

public class DebuggingRecyclerAdapter extends RecyclerView.Adapter<DebuggingRecyclerAdapter.ViewHolder>{
    private Context context;
    private List<MahasiswaDebugging> mahasiswaDebuggingList;

    public List<MahasiswaDebugging> getMahasiswaList() {

        return mahasiswaDebuggingList;
    }

    public void setMahasiswaList(List<MahasiswaDebugging> mahasiswaList) {
        this.mahasiswaDebuggingList = mahasiswaList;
        notifyDataSetChanged();
    }

    public DebuggingRecyclerAdapter(Context context) {
        this.context = context;
        mahasiswaDebuggingList = new ArrayList<>();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.item_list_debugging,parent,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        MahasiswaDebugging m = mahasiswaDebuggingList.get(position);

        holder.textNama.setText(m.getNama());
        holder.textTelp.setText(m.getNotelp());
        holder.textNim.setText(m.getNim());
    }

    @Override
    public int getItemCount() {

        return mahasiswaDebuggingList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView textNama, textNim, textTelp;

        public ViewHolder(@NonNull View itemView){
            super(itemView);
            textNama = itemView.findViewById(R.id.textNama);
            textNim = itemView.findViewById(R.id.textNidn);
            textTelp = itemView.findViewById(R.id.textTelp);
        }
    }
}
