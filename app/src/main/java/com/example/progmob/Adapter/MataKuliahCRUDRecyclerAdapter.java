package com.example.progmob.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.progmob.Model.MataKuliah;
import com.example.progmob.R;

import java.util.ArrayList;
import java.util.List;

public class MataKuliahCRUDRecyclerAdapter extends RecyclerView.Adapter<MataKuliahCRUDRecyclerAdapter.ViewHolder> {
    private Context context;
    private List<MataKuliah> mataKuliahList;

    public List<MataKuliah> getMataKuliahList() {
        return mataKuliahList;
    }

    public void setMataKuliahList(List<MataKuliah> mataKuliahList) {
        this.mataKuliahList = mataKuliahList;
        notifyDataSetChanged();
    }

    public MataKuliahCRUDRecyclerAdapter(Context context) {
        this.context = context;
        mataKuliahList = new ArrayList<>();
    }
    public MataKuliahCRUDRecyclerAdapter(List<MataKuliah> mataKuliahList){
        this.mataKuliahList = mataKuliahList;
    }

    @NonNull
    @Override
    public MataKuliahCRUDRecyclerAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_matakuliah,parent,false);
        return new MataKuliahCRUDRecyclerAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MataKuliahCRUDRecyclerAdapter.ViewHolder holder, int position) {
        MataKuliah mt = mataKuliahList.get(position);

        holder.textNama.setText(mt.getNama());
        holder.textKode.setText(mt.getKode());
        holder.textHari.setText(mt.getHari());
        holder.textSesi.setText(mt.getSesi());
        holder.textSks.setText(mt.getSks());
    }

    @Override
    public int getItemCount() {

        return mataKuliahList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView textNama, textKode, textHari, textSesi, textSks;

        public ViewHolder(@NonNull View itemView){
            super(itemView);
            textNama = itemView.findViewById(R.id.textNama);
            textKode = itemView.findViewById(R.id.textKode);
            textHari = itemView.findViewById(R.id.textHari);
            textSesi = itemView.findViewById(R.id.textSesi);
            textSks = itemView.findViewById(R.id.textSks);
        }
    }



}
