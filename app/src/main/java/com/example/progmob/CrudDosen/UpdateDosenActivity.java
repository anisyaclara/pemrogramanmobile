package com.example.progmob.CrudDosen;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import com.example.progmob.Model.DefaultResult;
import com.example.progmob.Network.GetDataService;
import com.example.progmob.Network.RetrofitClientInstance;
import com.example.progmob.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdateDosenActivity extends AppCompatActivity {
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_dosen);

        EditText editNidnLama = (EditText)findViewById(R.id.editTextNidnLama);
        EditText editNamaBaru = (EditText)findViewById(R.id.editTextNamaBaru);
        EditText editNidnBaru = (EditText)findViewById(R.id.editTextNidnBaru);
        EditText editAlamatBaru = (EditText)findViewById(R.id.editTextAlamatBaru);
        EditText editEmailBaru = (EditText)findViewById(R.id.editTextEmailBaru);
        EditText editGelarBaru = (EditText)findViewById(R.id.editTextGelarBaru);
        Button btnUpdate = (Button)findViewById(R.id.btnUpdateDosen);
        pd = new ProgressDialog(UpdateDosenActivity.this);

        btnUpdate.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                pd.setTitle("Mohon Bersabar");
                pd.show();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> delete = service.delete_dosen(editNidnLama.getText().toString(),"72180260");

                delete.enqueue(new Callback<DefaultResult>(){
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response){
                        Toast.makeText(UpdateDosenActivity.this, "Data dihapus", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t){
                        pd.dismiss();
                        Toast.makeText(UpdateDosenActivity.this, "Gagal dihapus", Toast.LENGTH_LONG).show();
                    }
                });

                Call<DefaultResult> call = service.add_dosen(
                        editNamaBaru.getText().toString(),
                        editNidnBaru.getText().toString(),
                        editAlamatBaru.getText().toString(),
                        editEmailBaru.getText().toString(),
                        editGelarBaru.getText().toString(),
                        "kosongkan saja diisi random oleh sistem",
                        "72180260"
                );

                call.enqueue(new Callback<DefaultResult>(){
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response){
                        pd.dismiss();
                        Toast.makeText(UpdateDosenActivity.this, "Data sudah diupdate", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t){
                        pd.dismiss();
                        Toast.makeText(UpdateDosenActivity.this, "Gagal diupdate", Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }
}