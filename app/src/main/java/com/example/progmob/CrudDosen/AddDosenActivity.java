package com.example.progmob.CrudDosen;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.progmob.Model.DefaultResult;
import com.example.progmob.Network.GetDataService;
import com.example.progmob.Network.RetrofitClientInstance;
import com.example.progmob.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddDosenActivity extends AppCompatActivity {
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_dosen);

        final EditText edNama = (EditText)findViewById(R.id.editTextNama);
        final EditText edNidn = (EditText)findViewById(R.id.editTextNidn);
        final EditText edAlamat = (EditText)findViewById(R.id.editTextAlamat);
        final EditText edEmail = (EditText)findViewById(R.id.editTextEmail);
        final EditText edGelar = (EditText)findViewById(R.id.editTextGelar);
        Button btnSimpan = (Button)findViewById(R.id.btnSimpanDosen);
        pd = new ProgressDialog(AddDosenActivity.this);

        btnSimpan.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                pd.setTitle("Mohon Menunggu");
                pd.show();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> call = service.add_dosen(
                        edNama.getText().toString(),
                        edNidn.getText().toString(),
                        edAlamat.getText().toString(),
                        edEmail.getText().toString(),
                        edGelar.getText().toString(),
                        "Kosongkan saja diisi sembarang karena dirandom sistem",
                        "72180260"
                );

                call.enqueue(new Callback<DefaultResult>(){
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response){
                        pd.dismiss();
                        Toast.makeText(AddDosenActivity.this, "DATA BERHASIL DISIMPAN", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t){
                        pd.dismiss();
                        Toast.makeText(AddDosenActivity.this, "GAGAL", Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }
}