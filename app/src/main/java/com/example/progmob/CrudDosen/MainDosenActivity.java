package com.example.progmob.CrudDosen;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.example.progmob.R;
public class MainDosenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_dosen);

        Button btnTambah= (Button)findViewById(R.id.btnAddDosen);
        Button btnLihat = (Button)findViewById(R.id.btnLihatDosen);
        Button btnUbah = (Button)findViewById(R.id.btnUpdateDosen);
        Button btnHapus = (Button)findViewById(R.id.btnHapusDosen);

        btnLihat.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent intent = new Intent(MainDosenActivity.this, LihatDosenActivity.class);
                startActivity(intent);
            }
        });

        btnTambah.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent intent = new Intent(MainDosenActivity.this, AddDosenActivity.class);
                startActivity(intent);
            }
        });

        btnHapus.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent intent = new Intent(MainDosenActivity.this, DeleteDosenActivity.class);
                startActivity(intent);
            }
        });

        btnUbah.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent intent = new Intent(MainDosenActivity.this, UpdateDosenActivity.class);
                startActivity(intent);
            }
        });
    }

}