package com.example.progmob.CrudDosen;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.progmob.Adapter.DosenCRUDRecyclerAdapter;
import com.example.progmob.Model.Dosen;
import com.example.progmob.Network.GetDataService;
import com.example.progmob.Network.RetrofitClientInstance;
import com.example.progmob.R;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LihatDosenActivity extends AppCompatActivity {
    RecyclerView rvDosen;
    DosenCRUDRecyclerAdapter dosenAdapter;
    ProgressDialog pd;
    List<Dosen> dosenList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lihat_dosen);

        rvDosen = (RecyclerView)findViewById(R.id.rvLihatDosen);
        pd = new ProgressDialog(this);
        pd.setTitle("Mohon Bersabar");
        pd.show();

        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<List<Dosen>> call = service.getDosen("72180260");

        call.enqueue(new Callback<List<Dosen>>() {
            @Override
            public void onResponse(Call<List<Dosen>> call, Response<List<Dosen>> response){
                pd.dismiss();
                dosenList = response.body();
                dosenAdapter = new DosenCRUDRecyclerAdapter(dosenList);

                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(LihatDosenActivity.this);
                rvDosen.setLayoutManager(layoutManager);
                rvDosen.setAdapter(dosenAdapter);
            }
            @Override
            public void onFailure(Call<List<Dosen>> call, Throwable t){
                pd.dismiss();
                Toast.makeText(LihatDosenActivity.this, "Error", Toast.LENGTH_LONG).show();
            }
        });
    }
}