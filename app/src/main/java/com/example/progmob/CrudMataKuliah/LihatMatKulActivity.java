package com.example.progmob.CrudMataKuliah;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.progmob.Adapter.MataKuliahCRUDRecyclerAdapter;
import com.example.progmob.Model.MataKuliah;
import com.example.progmob.Network.GetDataService;
import com.example.progmob.Network.RetrofitClientInstance;
import com.example.progmob.R;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LihatMatKulActivity extends AppCompatActivity {
    RecyclerView rvMatKul;
    MataKuliahCRUDRecyclerAdapter mataKuliahAdapter;
    ProgressDialog pd;
    List<MataKuliah> mataKuliahList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lihat_mat_kul);

        rvMatKul = (RecyclerView)findViewById(R.id.rvLihatMatKul);
        pd = new ProgressDialog(this);
        pd.setTitle("Mohon Bersabar");
        pd.show();

        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<List<MataKuliah>> call = service.getMataKuliah("72180260");

        call.enqueue(new Callback<List<MataKuliah>>() {
            @Override
            public void onResponse(Call<List<MataKuliah>> call, Response<List<MataKuliah>> response){
                pd.dismiss();
                mataKuliahList = response.body();
                mataKuliahAdapter = new MataKuliahCRUDRecyclerAdapter(mataKuliahList);

                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(LihatMatKulActivity.this);
                rvMatKul.setLayoutManager(layoutManager);
                rvMatKul.setAdapter(mataKuliahAdapter);
            }
            @Override
            public void onFailure(Call<List<MataKuliah>> call, Throwable t){
                pd.dismiss();
                Toast.makeText(LihatMatKulActivity.this, "Error", Toast.LENGTH_LONG).show();
            }
        });
    }
}