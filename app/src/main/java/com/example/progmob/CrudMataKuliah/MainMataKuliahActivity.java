package com.example.progmob.CrudMataKuliah;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.example.progmob.R;

public class MainMataKuliahActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_mata_kuliah);

        Button btnLihat= (Button)findViewById(R.id.btnLihatMatkul);
        Button btnTambah= (Button)findViewById(R.id.btnAddMatkul);
        Button btnUpdate= (Button)findViewById(R.id.btnUbahMatkul);
        Button btnDelete = (Button)findViewById(R.id.btnHapusMatkul);

        btnLihat.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent intent = new Intent(MainMataKuliahActivity.this, LihatMatKulActivity.class);
                startActivity(intent);
            }
        });
        btnTambah.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent intent = new Intent(MainMataKuliahActivity.this, AddMatKulActivity.class);
                startActivity(intent);
            }
        });
        btnUpdate.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent intent = new Intent(MainMataKuliahActivity.this, UpdateMatKulActivity.class);
                startActivity(intent);
            }
        });
        btnDelete.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent intent = new Intent(MainMataKuliahActivity.this, DeleteMatKulActivity.class);
                startActivity(intent);
            }
        });

    }
}