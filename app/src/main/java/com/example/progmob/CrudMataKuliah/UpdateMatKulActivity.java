package com.example.progmob.CrudMataKuliah;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.progmob.Model.DefaultResult;
import com.example.progmob.Network.GetDataService;
import com.example.progmob.Network.RetrofitClientInstance;
import com.example.progmob.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdateMatKulActivity extends AppCompatActivity {
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_mat_kul);

        EditText editKodeLama = (EditText)findViewById(R.id.editTextKodeLama);
        EditText editNamaBaru = (EditText)findViewById(R.id.editTextNamaBaru);
        EditText editKodeBaru = (EditText)findViewById(R.id.editTextKodeBaru);
        EditText editHariBaru = (EditText)findViewById(R.id.editTextHariBaru);
        EditText editSesiBaru = (EditText)findViewById(R.id.editTextSesiBaru);
        EditText editSksBaru = (EditText)findViewById(R.id.editTextSksBaru);
        Button btnUpdate = (Button)findViewById(R.id.btnUbahMatKul);
        pd = new ProgressDialog(UpdateMatKulActivity.this);

        btnUpdate.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                pd.setTitle("Mohon Bersabar");
                pd.show();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> delete = service.delete_matkul(editKodeLama.getText().toString(),"72180260");

                delete.enqueue(new Callback<DefaultResult>(){
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response){
                        Toast.makeText(UpdateMatKulActivity.this, "Data dihapus", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t){
                        pd.dismiss();
                        Toast.makeText(UpdateMatKulActivity.this, "Gagal dihapus", Toast.LENGTH_LONG).show();
                    }
                });

                Call<DefaultResult> call = service.add_matkul(
                        editNamaBaru.getText().toString(),
                        editKodeBaru.getText().toString(),
                        editHariBaru.getText().toString(),
                        editSesiBaru.getText().toString(),
                        editSksBaru.getText().toString(),
                        "72180260"
                );

                call.enqueue(new Callback<DefaultResult>(){
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response){
                        pd.dismiss();
                        Toast.makeText(UpdateMatKulActivity.this, "Data sudah diupdate", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t){
                        pd.dismiss();
                        Toast.makeText(UpdateMatKulActivity.this, "Gagal diupdate", Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }
}