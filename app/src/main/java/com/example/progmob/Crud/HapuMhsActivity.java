package com.example.progmob.Crud;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.progmob.Model.DefaultResult;
import com.example.progmob.Network.GetDataService;
import com.example.progmob.Network.RetrofitClientInstance;
import com.example.progmob.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HapuMhsActivity extends AppCompatActivity {
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hapu_mhs);

        EditText editNim = (EditText)findViewById(R.id.editNim);
        Button btnDelete = (Button)findViewById(R.id.btnDeleteMhs);
        pd = new ProgressDialog(HapuMhsActivity.this);

        btnDelete.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                pd.setTitle("Mohon Menunggu");
                pd.show();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> call = service.delete_mhs(editNim.getText().toString(), "72180260");

                call.enqueue(new Callback<DefaultResult>(){
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response){
                        pd.dismiss();
                        Toast.makeText(HapuMhsActivity.this, "Data dihapus", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t){
                        pd.dismiss();
                        Toast.makeText(HapuMhsActivity.this, "Gagal dihapus", Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }
}