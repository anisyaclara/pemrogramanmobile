package com.example.progmob.Crud;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.progmob.Model.DefaultResult;
import com.example.progmob.Network.GetDataService;
import com.example.progmob.Network.RetrofitClientInstance;
import com.example.progmob.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdateMhsActivity extends AppCompatActivity {
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_mhs);

        EditText editNimLama = (EditText)findViewById(R.id.editTextNimLama);
        EditText editNamaBaru = (EditText)findViewById(R.id.editTextNamaBaru);
        EditText editNimBaru = (EditText)findViewById(R.id.editTextNimBaru);
        EditText editAlamatBaru = (EditText)findViewById(R.id.editTextAlamatBaru);
        EditText editEmailBaru = (EditText)findViewById(R.id.editTextEmailBaru);
        Button btnUpdate = (Button)findViewById(R.id.btnUpdateMhs);
        pd = new ProgressDialog(UpdateMhsActivity.this);

        btnUpdate.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                pd.setTitle("Mohon Bersabar");
                pd.show();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> delete = service.delete_mhs(editNimLama.getText().toString(),"72180260");

                delete.enqueue(new Callback<DefaultResult>(){
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response){
                        Toast.makeText(UpdateMhsActivity.this, "Data dihapus", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t){
                        pd.dismiss();
                        Toast.makeText(UpdateMhsActivity.this, "Gagal dihapus", Toast.LENGTH_LONG).show();
                    }
                });

                Call<DefaultResult> call = service.add_mhs(
                        editNamaBaru.getText().toString(),
                        editNimBaru.getText().toString(),
                        editAlamatBaru.getText().toString(),
                        editEmailBaru.getText().toString(),
                        "kosongkan saja diisi random oleh sistem",
                        "72180260"
                        );

                call.enqueue(new Callback<DefaultResult>(){
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response){
                        pd.dismiss();
                        Toast.makeText(UpdateMhsActivity.this, "Data sudah diupdate", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t){
                        pd.dismiss();
                        Toast.makeText(UpdateMhsActivity.this, "Gagal diupdate", Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }
}