package com.example.progmob.UTS;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import androidx.appcompat.app.AppCompatActivity;

import com.example.progmob.Crud.MainMhsActivity;
import com.example.progmob.CrudDosen.MainDosenActivity;
import com.example.progmob.CrudMataKuliah.MainMataKuliahActivity;
import com.example.progmob.Pertemuan6.PrefActivity;
import com.example.progmob.R;
public class HomeActivity extends AppCompatActivity {
    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        ImageButton btnDosen = (ImageButton)findViewById(R.id.btnDosen);
        ImageButton btnMahasiswa = (ImageButton)findViewById(R.id.btnMahasiswa);
        ImageButton btnMatKul = (ImageButton)findViewById(R.id.btnMatKul);
        Button btnLogout = (Button)findViewById(R.id.btnLogout);

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(HomeActivity.this);
        if (sharedPreferences.getString("nimnik", "").isEmpty() && sharedPreferences.getString("nama", "").isEmpty()) {
            finish();
            startActivity(new Intent(HomeActivity.this, PrefActivity.class));
            return;
        }

        btnDosen.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent intent = new Intent(HomeActivity.this, MainDosenActivity.class);
                startActivity(intent);
            }
        });
        btnMahasiswa.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent intent = new Intent(HomeActivity.this, MainMhsActivity.class);
                startActivity(intent);
            }
        });
        btnMatKul.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent intent = new Intent(HomeActivity.this, MainMataKuliahActivity.class);
                startActivity(intent);
            }
        });
        btnLogout.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.clear();
                editor.apply();
                finish();
                Intent intent = new Intent(HomeActivity.this, PrefActivity.class);
                startActivity(intent);
            }
        });
    }
}