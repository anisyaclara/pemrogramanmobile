package com.example.progmob.Pertemuan4;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

import com.example.progmob.Adapter.DebuggingRecyclerAdapter;
import com.example.progmob.Model.MahasiswaDebugging;
import com.example.progmob.R;

public class DebuggingActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_debugging);

        RecyclerView rvDebug = (RecyclerView)findViewById(R.id.rvDebug);
        DebuggingRecyclerAdapter debuggingRecyclerAdapter;

        //data dummy
        List<MahasiswaDebugging> mahasiswaDebuggingList = new ArrayList<MahasiswaDebugging>();

        //generate data mahasiswa
        MahasiswaDebugging m1 = new MahasiswaDebugging("Argo","72110101","084646464646");
        MahasiswaDebugging m2 = new MahasiswaDebugging("Halim","72110101","084646464646");
        MahasiswaDebugging m3 = new MahasiswaDebugging("Jong Jek Siang","72110101","084646464646");
        MahasiswaDebugging m4 = new MahasiswaDebugging("Katon","72110101","084646464646");
        MahasiswaDebugging m5 = new MahasiswaDebugging("Yetli","72110101","084646464646");

        mahasiswaDebuggingList.add(m1);
        mahasiswaDebuggingList.add(m2);
        mahasiswaDebuggingList.add(m3);
        mahasiswaDebuggingList.add(m4);
        mahasiswaDebuggingList.add(m5);

        debuggingRecyclerAdapter = new DebuggingRecyclerAdapter(DebuggingActivity.this);
        debuggingRecyclerAdapter.setMahasiswaList(mahasiswaDebuggingList);

        rvDebug.setLayoutManager(new LinearLayoutManager(DebuggingActivity.this));
        rvDebug.setAdapter(debuggingRecyclerAdapter);
    }
}
