package com.example.progmob.Pertemuan6;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.progmob.Model.User;
import com.example.progmob.Network.GetDataService;
import com.example.progmob.Network.RetrofitClientInstance;
import com.example.progmob.R;
import com.example.progmob.UTS.HomeActivity;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PrefActivity extends AppCompatActivity {
    //String isLogin = "";
    ProgressDialog pd;
    List<User> users;
    SharedPreferences session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pref);

        Button btnPref = (Button) findViewById(R.id.btnPref);
        EditText nimnik = (EditText) findViewById(R.id.textUserName);
        EditText password = (EditText) findViewById(R.id.textPassword);
        pd = new ProgressDialog(PrefActivity.this);

        Toast.makeText(PrefActivity.this, "LOGIN", Toast.LENGTH_SHORT).show();
        session = PreferenceManager.getDefaultSharedPreferences(PrefActivity.this);
        if(!session.getString("nimnik", "").isEmpty() && !session.getString("nama", "").isEmpty()){
            finish();
            startActivity(new Intent(PrefActivity.this, HomeActivity.class));
            return;
        }

        btnPref.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                pd.setTitle("Mohon Menunggu");
                pd.show();
                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<List<User>> call = service.login(
                        nimnik.getText().toString(),
                        password.getText().toString()
                );

                call.enqueue(new Callback<List<User>>(){
                    @Override
                    public void onResponse(Call<List<User>> call, Response<List<User>> response){
                        pd.dismiss();
                        users = response.body();
                        User u = users.get(0);
                        SharedPreferences.Editor editor = session.edit();
                        editor.clear();
                        editor.putString("nimnik", u.getNimnik());
                        editor.putString("nama", u.getNama());
                        editor.apply();
                        finish();
                        Intent intent = new Intent(PrefActivity.this, HomeActivity.class);
                        startActivity(intent);
                    }

                    @Override
                    public void onFailure(Call<List<User>> call, Throwable t){
                        pd.dismiss();
                        Toast.makeText(PrefActivity.this, "Tidak Dapat Login", Toast.LENGTH_LONG).show();
                        btnPref.setEnabled(false);
                    }
                });
            }
        });

    }
}
